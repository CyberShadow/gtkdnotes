# ListBox

A `gtk.ListBox` is a **vertical** container that contains `gtk.ListBoxRow` children. These **rows can be dynamically sorted and filtered, and headers can be added dynamically depending on the row content**. It also allows keyboard and mouse navigation and selection like a typical list.

Using `gtk.ListBox` is often an alternative to `gtk.TreeView`, especially when the list content has a more complicated layout than what is allowed by a `gtk.CellRenderer`, or when the content is interactive (i.e. has a button in it).

A `ListBox` expects you to add any number of `ListBoxRow`s.

Although a `gtk.ListBox` must have only gtkListBoxRow children, you can add any kind of widget to it via `add()` and a `gtk.ListBoxRow` widget will automatically be inserted between the list and the widget.

### Basic example:

```
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mainWindow = new MainWindow("ListBox Demo");
    scope (success)
        mainWindow.showAll();

    import gtk.Box : Box;
    import gtkc.gtktypes : GtkOrientation;

    auto mainBox = new Box(GtkOrientation.VERTICAL, 0);
    scope (success)
        mainWindow.add(mainBox);

    import gtk.ListBox : ListBox;
    import gtk.ListBoxRow : ListBoxRow;
    import gtk.Button : Button;
    import gtkc.gtktypes : GtkSelectionMode;

    auto listBox = new ListBox();
    scope (success) {
        listBox.showAll();
        mainBox.add(listBox);
    }
    // Make these listBoxRows below unselectable
    listBox.setSelectionMode(GtkSelectionMode.NONE);
    // ListBoxRows are to be added to the ListBox
    auto listBoxRow = new ListBoxRow();

    // gtk.ListBox is a vertical container.

    import std.array : split;

    foreach (buttonName; "This is a listbox container".split())
        listBox.add(() {
            // this lambda:
            // 1. creates a new LixtBoxRow
            // 2. attaches a new button with the button name
            // 3. returns it
            auto currentListRow = new ListBoxRow();
            currentListRow.add(new Button(buttonName));
            return currentListRow;
        }());

    listBox.showAll();
}
```

### A bit more elaborate example: 

(Note that the design below is not very suitable for real use, it's just) an 
example of how you can add boxes to the ListbBoxRows.)

```
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mainWindow = new MainWindow("ListBox Demo");
    scope (success)
        mainWindow.showAll();

    mainWindow.add(() {
        import gtk.ListBox : ListBox;
        import gtk.ListBoxRow : ListBoxRow;
        import gtkc.gtktypes : SelectionMode;

        auto mainList = new ListBox();
        mainList.setSelectionMode(SelectionMode.NONE);

        import gtk.Box : Box;
        import gtkc.gtktypes : GtkOrientation;
        import gtk.Label : Label;

        mainList.add(() {
            // automatic date time row
            auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);
            hbox.packStart(() {
                // left row (lables)
                auto vbox = new Box(GtkOrientation.VERTICAL, 0);
                vbox.packStart(new Label("Automatic Date & Time"), true, true, 0);
                vbox.packStart(new Label("(Requires internet access)"), true, true, 0);
                return vbox;
            }(), true, true, 0);

            hbox.packStart(() {
                // right row (switch)
                import gtk.Switch : Switch;

                return new Switch();
            }(), false, false, 0);
            return hbox;
        }());

        mainList.add(() {
            // enable automatic update row
            auto eaurow = new ListBoxRow();
            eaurow.add(() {
                auto box = new Box(GtkOrientation.HORIZONTAL, 5);
                box.packStart(() {
                    auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);
                    hbox.packStart(new Label("Enable Automatic Update"), true, true, 0);
                    hbox.packStart(() {
                        import gtk.CheckButton : CheckButton;

                        return new CheckButton();
                    }(), false, false, 0);
                    return hbox;
                }(), true, true, 0);
                return box;
            }());
            return eaurow;
        }());

        mainList.add(() {
            // date format row
            auto box = new Box(GtkOrientation.HORIZONTAL, 5);
            box.packStart(() {
                auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);
                hbox.packStart(new Label("Date format"), true, false, 0);
                hbox.packStart(() {
                    import gtk.ComboBoxText : ComboBoxText;

                    auto comboBox = new ComboBoxText();
                    comboBox.insert(0, "0", "AM/PM");
                    comboBox.insert(1, "0", "24h");
                    return comboBox;
                }(), false, false, 0);
                return hbox;
            }(), true, true, 0);
            return box;
        }());

        mainList.showAll();
        return mainList;
    }());
}
```
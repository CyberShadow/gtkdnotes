# HeaderBar

A `gtk.HeaderBar` is similar to a horizontal `gtk.Box`, it allows to place children at the start or the end. In addition, it allows a title to be displayed. The title will be centered with respect to the width of the box, even if the children at either side take up different amounts of space.

Since GTK+ now supports Client Side Decoration, a `gtk.HeaderBar` can be used in place of the title bar (which is rendered by the Window Manager).

A `gtk.HeaderBar` is usually located across the top of a window and should contain commonly used controls which affect the content below. They also provide access to window controls, including the close window button and window menu.

### Example:

```
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();
    auto mw = new MainWindow("This title will be overwritten");
    scope (success)
        mw.showAll();
    mw.setDefaultSize(300, 300);
    mw.setTitlebar(() {
        import gtk.HeaderBar : HeaderBar;

        auto hb = new HeaderBar();
        hb.setTitle("Your App");
        hb.setSubtitle("A Subtitle");

        hb.packEnd(() {
            import gtk.Button : Button;

            auto closeButton = new Button("Close");
            closeButton.addOnClicked(delegate void(Button _) { Main.quit(); });
            return closeButton;
        }());
        return hb;
    }());
}
```
